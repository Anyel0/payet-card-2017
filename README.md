﻿# Frontr Source

[![devDependency Status](https://david-dm.org/quintana-dev/frontr-src/dev-status.svg?style=flat-square)](https://david-dm.org/quintana-dev/frontr-src#info=devDependencies) [![Build Status](http://img.shields.io/travis/quintana-dev/frontr-src.svg?style=flat-square)](https://travis-ci.org/quintana-dev/frontr-src)

> Simple Front-End scaffolds to starting web apps.

*For generate your own web projects from terminal, try [Frontr CLI](https://github.com/quintana-dev/frontr).*

## Features

* [HTML5 Boilerplate](http://html5boilerplate.com/) front-end template.
* [Gruntjs](http://gruntjs.org/) for Server and Build tasks.
* [Bower](http://bower.io/) for dependencies managment.
* [Sass](http://http://sass-lang.com/) pre-processor (With [libsass](https://github.com/sass/libsass) compiler)
* [Bootstrap 3](http://getbootstrap.com/) CSS Framework v3 (or any you want)
* [jQuery](http://jquery.com/) JS Framework.
* [Modernizr](http://modernizr.com/) JS library.
* [PostCSS](https://github.com/postcss/postcss) and [Autoprefixer](https://github.com/postcss/autoprefixer)
* [ESLint](https://github.com/eslint/eslint) A fully pluggable tool for identifying and reporting on patterns in JavaScript.
* [Livereload](https://github.com/gruntjs/grunt-contrib-connect) and [watch files changes](https://github.com/gruntjs/grunt-contrib-watch)
* [Minify HTML](https://github.com/gruntjs/grunt-contrib-htmlmin)
* [Compress CSS files](https://github.com/gruntjs/grunt-contrib-cssmin)
* [Removing unused CSS from your projects](https://github.com/addyosmani/grunt-uncss)
* [Minify PNG and JPEG images](https://github.com/gruntjs/grunt-contrib-imagemin)
* [Minify files with UglifyJS](https://github.com/gruntjs/grunt-contrib-uglify)
* [File revving](https://github.com/yeoman/grunt-filerev)
* [Just In Time plugin loader for Grunt tasks](https://github.com/shootaroo/jit-grunt)
* [Mocha Tests](https://github.com/mochajs/mocha)

## Requirements
It's necessary to install the following packages before:

* [Nodejs](http://nodejs.org/) >= 0.10.0
* [Gruntjs](http://gruntjs.com/) >= 0.4.0
* [Bower](http://bower.io/) >= 1.3.10

## How to use

### Installation

Clone the repository or try [Frontr CLI](https://github.com/quintana-dev/frontr) for more cool installation.

```sh
$ git clone https://github.com/quintana-dev/frontr-src.git
```

Enter to project dir

```sh
$ cd frontr-src
```

Install local packages

```sh
$ npm install
```

### Usage

#### Serve
Start development server and preview the project located at `app/` dir.

```sh
$ grunt serve
```

#### Build
Generate a dist files at `dist/` dir.

```sh
$ grunt build
```

#### Dist
Run dist server with deployed files located at `dist/` dir.

```sh
$ grunt dist
```

#### Remote access

For remote access in network using `--remote-access` option.

```sh
$ grunt serve --remote-access
# or
$ grunt dist --remote-access
```

**Note:** For ***remote access*** in ***Windows OS*** change 0.0.0.0 to network IP address on your browser.

## Configuration

Simple commons setup via `frontr.json`.

```json
{
  "hostname": "localhost",
  "port": 7000,
  "app": "/app",
  "dist": "/dist",
  "assets": {
    "dirname": "/assets",
    "src": {
      "css": "",
      "js": "/js",
      "img": "/img",
      "fonts": "/fonts"
    },
    "style": "style"
  },
  "uncssIgnore": ".uncss-?\\S*"
}
```