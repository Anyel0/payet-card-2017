module.exports = function (grunt) {
  require('time-grunt')(grunt)
  require('jit-grunt')(grunt, {
    useminPrepare: 'grunt-usemin'
  })

  // Configuration
  var config = grunt.file.readJSON('frontr.json')

  config.app = '.' + config.app
  config.dist = '.' + config.dist

  config.assetsApp = config.app + config.assets.dirname
  config.assetsDistDir = config.dist + config.assets.dirname

  config.cssDir = config.assets.dirname + config.assets.src.css
  config.jsDir = config.assets.dirname + config.assets.src.js
  config.imgDir = config.assets.dirname + config.assets.src.img
  config.fontsDir = config.assets.dirname + config.assets.src.fonts

  grunt.initConfig({
    config: config,
    pkg: grunt.file.readJSON('package.json'),
    watch: {
      bower: {
        files: ['bower.json'],
        tasks: ['wiredep']
      },
      sass: {
        files: [
          '<%= config.assetsApp %>/<%= config.assets.style %>.{scss,sass}',
          '<%= config.assetsApp %>/scss/**/*.{scss,sass}'
        ],
        tasks: ['sass']
      },
      js: {
        files: ['<%= config.app %><%= config.jsDir %>/{,*/}*.js']
      },
      test: {
        files: ['test/spec/{,*/}*.js'],
        tasks: ['mocha']
      },
      gruntfile: {
        files: ['Gruntfile.js']
      }
    },
    browserSync: {
      options: {
        notify: false,
        background: true,
        watchOptions: {
          ignored: ''
        }
      },
      livereload: {
        options: {
          files: [
            '<%= config.app %>/{,*/}*.html',
            '<%= config.app %><%= config.imgDir %>/{,*/}*.{gif,ico,jpg,jpeg,png,svg,webp}',
            '.tmp/<%= config.assets.dirname %><%= config.assets.src.css %>/{,*/}*.css',
            '.tmp/<%= config.assets.dirname %><%= config.assets.src.js %>/{,*/}*.js'
          ],
          port: config.port,
          server: {
            baseDir: ['.tmp', config.app],
            routes: {
              '/bower_components': './bower_components'
            }
          }
        }
      },
      dist: {
        options: {
          port: config.port,
          background: false,
          server: '<%= config.dist %>'
        }
      },
      test: {
        options: {
          port: config.port + 2,
          host: config.hostname,
          files: [
            'test/{,*/}*.html',
            'test/spec/*.js'
          ],
          server: {
            baseDir: ['./test'],
            routes: {
              '/bower_components': './bower_components'
            }
          }
        }
      }
    },
    clean: {
      dist: {
        files: [{
          dot: true,
          src: [
            '.tmp',
            '<%= config.dist %>/*',
            '!<%= config.dist %>/.git*'
          ]
        }]
      },
      server: '.tmp'
    },
    sass: {
      options: {
        sourceMap: true,
        sourceMapEmbed: true,
        sourceMapContents: true,
        includePaths: ['.']
      },
      dist: {
        files: [{
          expand: true,
          cwd: '<%= config.assetsApp %>',
          src: ['<%= config.assets.style %>.{scss,sass}'],
          dest: '.tmp/<%= config.assets.dirname %><%= config.assets.src.css %>',
          ext: '.css'
        }]
      }
    },
    postcss: {
      options: {
        map: true,
        processors: [
          require('autoprefixer')({
            browsers: 'last 3 version'
          })
        ]
      },
      dist: {
        files: [{
          expand: true,
          cwd: '.tmp/<%= config.assets.dirname %><%= config.assets.src.css %>/',
          src: '{,*/}*.css',
          dest: '.tmp/<%= config.assets.dirname %><%= config.assets.src.css %>/'
        }]
      }
    },
    babel: {
      options: {
        sourceMap: true
      },
      dist: {
        files: [{
          expand: true,
          cwd: '<%= config.app %><%= config.jsDir %>',
          src: '{,*/}*.js',
          dest: '.tmp/<%= config.assets.dirname %><%= config.assets.src.js %>',
          ext: '.js'
        }]
      },
      test: {
        files: [{
          expand: true,
          cwd: 'test/spec',
          src: '{,*/}*.js',
          dest: '.tmp/test/spec',
          ext: '.js'
        }]
      }
    },
    wiredep: {
      app: {
        src: ['<%= config.app %>/*.html'],
        sass: {
          src: ['<%= config.assetsApp %>/<%= config.assets.style %>.{scss,sass}'],
          ignorePath: /^(\.\.\/)+/
        }
      }
    },
    copy: {
      dist: {
        files: [{
          expand: true,
          dot: true,
          cwd: '<%= config.app %>',
          dest: '<%= config.dist %>',
          src: [
            '{,*/}*.{html,ico,png,txt,xml,htaccess,appcache,mf}',
            '.<%= config.imgDir %>/**/*',
            '.<%= config.fontsDir %>/{,*/}*'
          ]
        }]
      },
      tmp: {
        files: [{
          expand: true,
          dot: true,
          cwd: '.tmp',
          dest: '<%= config.dist %>',
          src: [
            '.<%= config.assets.dirname %>/{,*/}*.css',
            '.<%= config.assets.dirname %><%= config.assets.src.js %>/{,*/}*.js'
          ]
        }]
      },
      test: {
        files: [{
          expand: true,
          dot: true,
          cwd: './test',
          dest: '.tmp/test',
          src: [
            '{,*/}*.html'
          ]
        }]
      },
      js: {
        files: [{
          expand: true,
          cwd: '<%= config.app %><%= config.jsDir %>',
          src: '{,*/}*.js',
          dest: '.tmp/<%= config.assets.dirname %><%= config.assets.src.js %>',
          ext: '.js'
        }]
      }
    },
    cssmin: {
      dist: {
        files: [{
          expand: true,
          cwd: '<%= config.dist %><%= config.cssDir %>/',
          src: [
            '*.css',
            '!*.min.css'
          ],
          dest: '<%= config.dist %><%= config.cssDir %>/',
          ext: '.css'
        }]
      }
    },
    htmlmin: {
      dist: {
        options: {
          collapseBooleanAttributes: true,
          collapseWhitespace: true,
          conservativeCollapse: true,
          removeAttributeQuotes: true,
          removeCommentsFromCDATA: true,
          removeEmptyAttributes: true,
          removeOptionalTags: true,
          removeRedundantAttributes: false,
          useShortDoctype: true
        },
        files: [{
          expand: true,
          cwd: '<%= config.dist %>',
          src: '{,*/}*.html',
          dest: '<%= config.dist %>'
        }]
      }
    },
    useminPrepare: {
      options: {
        dest: '<%= config.dist %>'
      },
      html: '<%= config.app %>/index.html'
    },
    usemin: {
      options: {
        assetsDirs: [
          '<%= config.dist %>',
          '<%= config.assetsDistDir %>'
        ]
      },
      html: ['<%= config.dist %>/{,*/}*.html'],
      css: ['<%= config.dist %>/<%= config.cssDir %>/{,*/}*.css']
    }
  })

  // Serve Tasks
  grunt.registerTask('serve', 'Start the development server and preview your app.' +
    ' --remote-access to allow remote access',
    function () {
      if (grunt.option('remote-access')) {
        grunt.config.set('connect.options.hostname', '0.0.0.0')
      }

      grunt.task.run([
        'clean:server',
        'wiredep',
        'sass',
        'postcss:dist',
        'browserSync:livereload',
        'watch'
      ])
    })

  // Dist Tasks
  grunt.registerTask('dist', 'Start the dist server and preview your app.' +
    '--remote-access to allow remote access',
    function () {
      if (grunt.option('remote-access')) {
        grunt.config.set('connect.options.hostname', '0.0.0.0')
      }

      grunt.task.run([
        'browserSync:dist',
        'watch'
      ])
    })

  // Build Tasks
  grunt.registerTask('build', function () {
    grunt.task.run([
      'clean:dist',
      'wiredep',
      'useminPrepare',
      'sass:dist',
      'copy:tmp',
      'copy:dist',
      'copy:js',
      'concat',
      'uglify',
      'postcss:dist',
      'cssmin:dist',
      'usemin'
    ])
  })

  // Test Tasks
  grunt.registerTask('test', function () {
    if (!grunt.option('watch')) {
      grunt.config.set('browserSync.test.options.open', false)
      grunt.config.set('browserSync.test.options.logLevel', 'silent')
      grunt.config.set('browserSync.test.options.files', [])
    }

    grunt.task.run([
      'browserSync:test',
      'mocha'
    ])

    if (grunt.option('watch')) {
      grunt.task.run([
        'watch:test'
      ])
    }
  })
}
