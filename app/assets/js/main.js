(function($) {
  var index = 0
  var timing = 3000
  var animate = false
  var $sectionList = $('section')
  var len = $sectionList.length
  var activeMusic = true

  var elemnto_y = 0;
  var elemnto_x = 0;

  $(window).resize(function() {
    setElementoGorro();
  });

  function setElementoGorro() {
    var  movil_y = 47;
    var  movil_x = 2;
    var min_x = 2;
    var min_y = 30;
    var max_x = 5;
    var max_y = 28;

    var hight_layer = $('body').height();
    var width_layer = $('body').width();
    var elementoGorro = $('#dot')
      // Calculamos el ancho y el alto


    if(hight_layer >= '579'){
      elemnto_y = $('body').height() * 4 + ($('#logo-footer').height() + ($('#logo-footer').height() / 2) - max_y);
      elemnto_x = ($(window).width() / 2) - (($('#logo-footer').width() / 2) + max_x);
      console.log('mayor');
    }else{
      elemnto_y = $('body').height() * 4 + ($('#logo-footer').height() + ($('#logo-footer').height() / 2) - min_y);
      elemnto_x = ($(window).width() / 2) - (($('#logo-footer').width() / 2) + max_x);
        console.log('menor');
    }

    if(width_layer < '600'){
      console.log('minimo ancho');
      elemnto_x = ($(window).width() / 2) - (($('#logo-footer').width() / 2) + min_x);
    }

    if(width_layer <= '375'){
      console.log('movíl');
      elemnto_y = $('body').height() * 4 + ($('#logo-footer').height() + ($('#logo-footer').height() / 2) - movil_y);
      elemnto_x = ($(window).width() / 2) - (($('#logo-footer').width() / 2) + movil_x);
    }

    // Aplicamos el ancho y el ancho
    elementoGorro.removeAttr('style');
    elementoGorro.css({
      'transform': 'matrix(0.95105, -0.30901, 0.30901, 0.95105, ' + elemnto_x + ', ' + elemnto_y + ')'
    });
    console.log(elemnto_y + ' ' + elemnto_x);
  }

  function scrollByIndex(i) {
    const el = $($sectionList[i])
    scrollTo(el.length > 0 ? el.offset().top : 0)
  }

  function scrollTo(y) {
    animate = true

    $('html,body').stop().animate({
      scrollTop: y
    })

    setElementoGorro();

    if (index == 4) {
      $('#end-title').addClass('title_type')
      $('.bg-branches-right, .bg-branches-left').fadeOut()
        //  TweenMax.from('#logo-show',6,{ x:600,rotation:360, sacale: 0.5 });
    }

    if (index < len - 1) {
      setTimeout(function() {
        animate = false

        setTimeout(function() {
          toDown()
        }, 2000)
      }, timing)
    } else {
      timing = 0
    }
  }

  function toUp() {
    if (index > 0) {
      index--
      scrollByIndex(index)
    } else {
      animate = false
    }
  }

  function toDown() {
    if (index < len - 1) {
      index++
      scrollByIndex(index)
    } else {
      animate = false
    }

    if (index === 1) {
      if (activeMusic) {
        // playSong();
        activeMusic = false
      }

      setTimeout(function() {
        $('#btn-next').hide()
      }, 600)
    }
  }

  /**
   * Event definitions
   * @return
   */
  function addEvents() {
    $(document).keydown(function(e) {
      if (e.which === 38 || e.which === 40) {
        e.preventDefault()
      }
    })

    $('body').on('mousewheel', function(e) {
      e.preventDefault()
    })

    $('#btn-next').on('click', function(e) {
      e.preventDefault()
      $('#btn-next').off('click')

      if (!animate) {
        $('html,body').addClass('scroll')

        setTimeout(function() {
          runAnimate1()
          toDown()
        }, 1000)
      }
    })
  }

  function runAnimate1() {
    var duration = 18
    var w = $(window).width() / 3
    var h = $('body').height()
    var n = 100
    var $hat = $('<div />', {
      id: 'dot'
    }).addClass('dot')

    var defaults = {
      x: w * 2 - 30,
      y: $(window).height() - 40
    }

    //var logoFooterX = ($(window).width() / 2) - (($('#logo-footer').width() / 2) + 30)
    setElementoGorro();

    var path = [{
      x: w + n ,
      y: h * 2 + n
    },{
      x: w  - 150,
      y: h * 2  + 200
    }, {
      x: w - 200 ,
      y: h * 3 - 200
    },{
      x: w - n  + 30 ,
      y: h * 3 - n  + 700
    }, {
      x: elemnto_x,
      y: elemnto_y
    }]

    $hat.appendTo('body')

    TweenMax.to($hat, 0, {
      bezier: [defaults],
      ease: Linear.easeNone,
      rotation: "360"
    })

    TweenMax.to($hat, duration, {
      bezier: path,
      ease: Linear.easeNone,
      rotation: -18,
      delay: 1,
      repeatDelay: 1,
      yoyo: true,
      immediateRender: false
    })
  }

  /**
   * Plugin definitions
   * @return
   */
  function addScrollReveal() {
    window.sr = new window.scrollReveal({
      after: '0',
      enter: 'top',
      move: '100px',
      over: '0.66s',
      easing: 'ease-in-out',
      viewportFactor: 0,
      reset: false,
      init: true
    })
  }

  /**
   * Run the app
   * @return
   */
  function run() {
    addEvents()
    addScrollReveal()
    setTimeout(function() {
      $('body,html').scrollTop(0)
    }, 400)
  }

  $(window).load(function() {
    run()
    console.log('OK!')
  })
})(window.jQuery)
